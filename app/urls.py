from django.urls import path, include
from .views import home, contact, galery, agregar_producto, listar_productos, modificar_producto, eliminar_producto, registro,ProductoViewset, MarcaViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('producto', ProductoViewset)
router.register('marca', MarcaViewset)

urlpatterns = [
    path('', home, name="home"),
    path('contact/', contact, name="contact"),
    path('galery/', galery, name="galery"),
    path('agregar-producto/', agregar_producto, name="agregar_producto"),
    path('listar-producto/', listar_productos, name="listar_productos"),
    path('modificar-producto/<id>/', modificar_producto, name="modificar_producto"),
    path('eliminar-producto/<id>/', eliminar_producto, name="eliminar_producto"),
    path('registro/', registro, name="registro"),
    path('api/', include(router.urls)),
]